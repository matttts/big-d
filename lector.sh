#!/bin/bash

if [ $# == 4 ]; then
	exec 10<&0
	exec < $1
	let cont=0

	echo "Resultados $3 en el archivo $4" > $2
	echo " " >> $2
	while read LINE; do
		let cont=cont+1
		echo "Patron $cont $LINE"
		echo $LINE >> $2
		{ time ./$3 $4 "$LINE" | grep "user" ; } 2>> $2
		echo " " >> $2
	done

	exec 0<&10 10<&-
else
	echo "usage: ./lector <patterns_file> <output> <algorithm> <file>"
fi
