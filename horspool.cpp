#include<iostream>
#include<string.h>
#include<time.h>
#include<stdio.h>
#include<stdlib.h>
#include<sys/stat.h>
#include<fstream>
#define MAXT 1001
#define MAXP 1000

using namespace std;

int posicion=0;
int cantidad=0;
char        patron[MAXP] ;
signed int  rep  ;            // numero de veces encontrado 
signed int  comp ;            // numero de comparaciones 

/******************** Busqueda por Boyer Moore Horspool ****************************/

void preBMBc(const char *P, int m, int tabla[])
{
    int i;
    for (i = 0; i < 256; ++i)
        tabla[i] = m;                  

    for (i = 0; i < m - 1; ++i)
    {
        tabla[P[i]] = m - i - 1;  

    }  

} 

void BMH(const char *T, int n , char *P, int m)
{
    int  j , bmBC[256] ;
    char c ;

    preBMBc(P, m, bmBC) ;  // Preprocesamiento 

    // Búsqueda 

    j = 0;
    while (j <= n - m)
    {

		if(T[j + m - 1] > 0)
		{
			c = T[j + m - 1] ;

			if ( P[m - 1] == c && memcmp(P, T + j, m - 1) == 0 )
			{
				 cout<<" * Encontrado en : "<< j+posicion << endl; 
				 cantidad++;
				 rep ++ ;
			}
			j = j + bmBC[c] ;     comp ++ ;                                   
		}
		else
			j++;
    }
}

/*************************** Funcion Principal *****************************/

int main(int argc, char* argv[])
{

    float t1 , t2, tiempo ;  // variables pata tiempo de busqueda
    rep = 0 ;
	
	if (argc != 3) {
		cout << "usage: " << argv[0] << " <input>" << " <patron>" << endl;
		return 0;
	}
	
    
    string line;
    ifstream myfile (argv[1]);
    //sscanf(argv[2], "%s", patron);
    //strcpy(argv[2],patron);
    //cout << patron << endl;
    //cout << argv[2] << endl;
    int n,m;
    
    if (myfile.is_open())
	{
		while ( getline (myfile,line) )
		{
			n  = line.size() ;
			m  = strlen( argv[2] ) ;
			BMH( line.c_str() , n , argv[2] , m ) ;
			posicion += n;
		}
		myfile.close();
	}
	
	cout << "OCURRENCIAS: " << cantidad << endl;
    
    return 0;
}
