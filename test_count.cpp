#include "ssa.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

using namespace std;

int main(int argc, char ** argv) {
  if(argc!=3) {
    cout << "usage: " << argv[0] << " <index-file> <pattern>" << endl;
    return 0;
  }

  ifstream ssainput(argv[1]);
  ssa * _ssa = new ssa(ssainput);
  ssainput.close();
  
  uint m = strlen(argv[2]);
  uchar * pattern = new uchar[m+1];
  
  for(uint k=0;k<m;k++)
	pattern[k] = argv[2][k];

  for(uint k=0;k<m;k++)
	cout << pattern[k];
  cout << endl << m << endl;

  uint occ = _ssa->count(pattern,m);
  
  if(occ > 0)
  {
	  uint *res;
	  
	  uint cant = _ssa->locate(pattern,m,&res);
	  for(uint i=0; i<cant; i++)
		cout << "ENCONTRADO EN LA POSICION: " << res[i] << endl;
	}

  cout << "OCURRENCIAS: " << occ << endl;
  delete _ssa;

  return 0;
}
