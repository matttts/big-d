import random

f = open("english.200MB","r")
pat4 = open("pat4.txt","w")
pat8 = open("pat8.txt","w")
pat16 = open("pat16.txt","w")

texto = f.read()

largo = len(texto)

cuatro=[]
ocho=[]
dieciseis=[]

for i in [4,8,16]:
	for j in range(0,10000):
		if i==4:
			pto = random.randint(largo/5,largo/5+1000000)
			while(texto[pto:pto+4] in cuatro or texto[pto:pto+4] == "    " or "\n" in texto[pto:pto+4]):
				pto = random.randint(largo/5,largo/5+1000000)
			cuatro.append(texto[pto:pto+4])
		elif i==8:
			pto = random.randint(largo/5,largo/5+1000000)
			while(texto[pto:pto+8] in ocho or texto[pto:pto+8] == "        " or "\n" in texto[pto:pto+8]):
				pto = random.randint(largo/5,largo/5+1000000)
			ocho.append(texto[pto:pto+8])
		elif i==16:
			pto = random.randint(largo/5,largo/5+1000000)
			while(texto[pto:pto+16] in dieciseis or texto[pto:pto+16] == "                " or "\n" in texto[pto:pto+16]):
				pto = random.randint(largo/5,largo/5+1000000)
			dieciseis.append(texto[pto:pto+16])

#~ for i in [4,8,16]:
	#~ for j in range(0,10000):
		#~ if i==4:
			#~ pto = random.randint(largo/4,largo/4+500000)
			#~ while(pto in cuatro):
				#~ pto = random.randint(largo/4,largo/4+500000)
			#~ cuatro.append(pto)
		#~ elif i==8:
			#~ pto = random.randint(largo/4,largo/4+500000)
			#~ while(pto in ocho):
				#~ pto = random.randint(largo/4,largo/4+500000)
			#~ ocho.append(pto)
		#~ elif i==16:
			#~ pto = random.randint(largo/4,largo/4+500000)
			#~ while(pto in dieciseis):
				#~ pto = random.randint(largo/4,largo/4+500000)
			#~ dieciseis.append(pto)
#~ 
#~ for i in range(0,10000):
	#~ pat4.write(texto[cuatro[i]:cuatro[i]+4])
	#~ pat4.write("\n")
	#~ pat8.write(texto[ocho[i]:ocho[i]+8])
	#~ pat8.write("\n")
	#~ pat16.write(texto[dieciseis[i]:dieciseis[i]+16])
	#~ pat16.write("\n")

for i in range(0,10000):
	pat4.write(cuatro[i])
	pat4.write("\n")
	pat8.write(ocho[i])
	pat8.write("\n")
	pat16.write(dieciseis[i])
	pat16.write("\n")

pat4.close()
pat8.close()
pat16.close()
