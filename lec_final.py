import sys

f = open(sys.argv[1])

linea = f.readline()
cant = 0
nlinea = 0
minutos = 0.0
segundos = 0.0
tiempo = 0.0

while linea != "":
	nlinea += 1
	if "real\t" in linea:
		for i in range(5,len(linea)):
			if linea[i] == "m":
				minutos += float(linea[5:i])
				for j in range(i+1,len(linea)):
					if linea[j] == "s":
						segundos += float(linea[i+1:j])
						break
				break
		cant += 1
	linea = f.readline()
	
minutos *= 60.0

if minutos+segundos > 60:
	a=(minutos+segundos)/60.0
	if a > 60:
		a=a/60.0
		print "Tiempo total: {} horas".format(a)
	else:
		print "Tiempo total: {} minutos".format(a)
else:
	print "Tiempo total: {} segundos".format(minutos+segundos)

tiempo = (minutos+segundos)/10000.0
if tiempo >= 60.0:
	tiempo /= 60.0
	print "Tiempo promedio: {} minutos".format(tiempo)
else:
	print "Tiempo promedio: {} segundos".format(tiempo)
	
f.close()
