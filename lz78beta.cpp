#include <iostream>
#include <map>
#include <fstream>
#include <vector>
#include <string.h>

#include <libcdsBasics.h>
#include <Array.h>
#include <cppUtils.h>
#include <BitSequence.h>
#include <BitSequenceRG.h>

using namespace std;
using namespace cds_utils;
using namespace cds_static;

using namespace std;
using namespace cds_utils;

void rew(Array *p, Array *n, BitSequenceRG *bsrg, char *pat, int pref, int padre, uint cont, uint pasos)
{
	if(pref>0 && cont>0)
	{
		if(n->getField(pref-1) == int(pat[cont]))
		{
			rew(p,n,bsrg,pat,p->getField(pref-1),padre,cont-1,pasos+1);
			rew(p,n,bsrg,pat,p->getField(pref-1),padre,cont,pasos+1);
		}
		else
		{
			rew(p,n,bsrg,pat,p->getField(pref-1),padre,strlen(pat)-1,pasos+1);
		}
	}
	else if(cont==0 && pref > 0)
	{
		if(int(pat[cont]) == n->getField(pref-1))
		{
			cout << "Encontrado en el prefijo: " << padre;
			if(bsrg->select1(padre+1) > bsrg->getLength())
				cout << ", posicion en el texto: " << bsrg->getLength()-(pasos+1) << endl;
			else
				cout << ", posicion en el texto: " << bsrg->select1(padre+1)-(pasos+1) << endl;
		}
	}
}

void occ(int raiz, Array *p, Array *n, uint &ocurrencias, BitSequenceRG *bsrg, char *pat)
{
	for(uint i=raiz;i<p->getLength();i++)
	{
		if(p->getField(i) == raiz)
		{
			if(strlen(pat) > 1)
				rew(p,n,bsrg,pat,i+1,i+1,strlen(pat)-1,0);
			occ(i+1,p,n,ocurrencias,bsrg,pat);
			ocurrencias++;
		}
	}
}

void lettermatch(int i,int cont, Array *p, Array *n, char *pat, int raiz, int ciclo, uint &ocurrencias, BitSequenceRG *bsrg)
{
	if(cont >= 0 && n->getField(i) == int(pat[cont]))
	{
		if(cont != 0 && p->getField(i) != 0)
			lettermatch(p->getField(i)-1,cont-1,p,n,pat,raiz,ciclo+1,ocurrencias,bsrg);
		else if(cont == 0)
		{
			cout << "Encontrado en el prefijo: " << raiz+1;
			if(bsrg->select1(raiz+2) > bsrg->getLength())
				cout << ", posicion en el texto: " << bsrg->getLength()-strlen(pat) << endl;
			else
				cout << ", posicion en el texto: " << bsrg->select1(raiz+2)-strlen(pat) << endl;
			ocurrencias++;
			occ(raiz+1,p,n,ocurrencias,bsrg,pat);
		}
	}
}

int main(int argc, char **argv) {
	if (argc != 3) {
		cout << "usage: " << argv[0] << " <input>" << " <patron>" << endl;
		return 0;
	}
	char *name_input = argv[1];
	char *patron = argv[2];
	uint ocurrencias=0;
	
	cout << "Largo: " << strlen(patron) << endl << patron  << endl;
	

	ifstream in(name_input);
	if (!in.good()) {
		cerr << "Error reading input file" << endl;
		return 1;
	}

	Array *prev = new Array(in);
	Array *news = new Array(in);
	BitSequenceRG *bsrg = BitSequenceRG::load(in);
	
	//for(uint i=0;i<prev->getLength();i++)
		//cout << i+1 << "(" << prev->getField(i) << "," << char(news->getField(i)) << ")" << endl;
	
	//cout << "BITMAP. LARGO: " << bsrg->getLength() << endl;
	//for(uint i=0;i<bsrg->getLength();i++)
		//cout << bsrg->access(i);
	//cout << endl;
	
	uint cont=strlen(patron)-1;
	
	if(cont > 0)
	{
		for(uint i=0;i<news->getLength();i++)
		{
			if(news->getField(i) == patron[cont] && prev->getField(i) != 0)
			{
				lettermatch(prev->getField(i)-1,cont-1,prev,news,patron,i,1,ocurrencias,bsrg);
			}
		}
	}
	else
	{
		for(uint i=0;i<news->getLength();i++)
		{
			if(news->getField(i) == patron[cont])
			{
				cout << "Encontrado en el prefijo: " << i+1;
				if(bsrg->select1(i+2) > bsrg->getLength())
					cout << ", posicion en el texto: " << bsrg->getLength()-strlen(patron) << endl;
				else
					cout << ", posicion en el texto: " << bsrg->select1(i+2)-strlen(patron) << endl;
				ocurrencias++;
				occ(i+1,prev,news,ocurrencias,bsrg,patron);
			}
		}
	}
	
	cout << "OCURRENCIAS: " << ocurrencias << endl;
	
	return 0;
}
